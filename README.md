# keeb-page

Source for the best [German keyboard page](https://keebs.nerdno.de/) on the planet.

Everything is licensed as [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1) unless noted otherwise.

## Installation & Building

Everything on the `live` branch is automatically deployed through [netlify](https://netlify.com).

For manual builds please use [Nikola](https://getnikola.com/):

### Install 
- Install Python 3.x and run `pip install -r requirements.txt` to get the dependencies
- Or just use `nix-shell` when you have nix installed

### Building
Run `nikola build` to render the page to html into the *output/* folder or run `nikola auto` to have a live preview in your browser at [localhost:8000](http://localhost:8000) which automatically rebuilds when you change a file.

## Adding content

Run `nikola new_page` to create a new page.
Add it to the navigation in `conf.py` afterwards, otherwise it won't show up.
