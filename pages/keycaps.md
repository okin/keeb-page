<!--
.. title: Keycaps
.. slug: keycaps
.. date: 2022-02-02 16:28:56 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->


Keycaps sind eine einfache Möglichkeit eine Tastatur nach eigenen Wünschen anzupassen.
Keycaps unterscheiden sich im verwendeten Profil, Material und der Farbgebung.

Meistens werden zugehörige Keycaps als Set angeboten, seltener kann man einzelne Keycaps kaufen.

# Alles MX oder was?

Keycaps werden auf Switches aufgesteckt, genauer gesagt sitzt die Kappe auf dem Schaft.
Das setzt voraus, dass diese beide Teile für eine gemeinsame Verbindung ausgelegt sind.
Die gängiste Art ist der kreuzförmige Schaft im Stile von Cherry MX.
Daneben ist Topre eine andere Art der Steckverbindung, bei welcher der Tausch mit entsprechenden Kappen möglich ist.

Es gibt daneben unzählige weitere Verbindungsarten, aber leider sind die wenigsten davon so weit verbreitet, dass ein einfacher Tausch möglich ist.

# Physikalische Layouts

## ISO oder ANSI?

Was die meisten deutschsprachigen Benutzer als Tastaturlayout kennen, wird als **ISO-DE** bezeichnet.

In der Hobbyszene finden sich oftmals als Standard **ANSI**-Sets, die sich allerdings nicht ohne Einschränkungen verwenden lassen.

Projekte wie [EurKEY](https://eurkey.steffen.bruentjen.eu/) versuchen das physikalische ANSI-Layout auch für europäische Sprachen nutzbar zu machen.

Es gibt Keycap-Sets mit ANSI Layout und deutschen Tasten, welche als **ANSI-DE** bezeichnet werden.
Diese sind allerdings eine Seltenheit.

### Unterschiede

* Linkes Shift ist bei ISO-DE kürzer und rechts davon findet sich eine Taste mit Sonderzeichen. Dadurch hat ein ISO-Layout eine Taste mehr.
* Bei ISO wird ein großer Enter-Knopf verwendet

### Geeignete Kits

Es lohnt sich ein Blick auf der verfügbaren Tasten oder auch die Bennenung der Kits.
Gängige Bezeichnungen für Kits, welche Support für deutsche Tasten enthalten sind **ISO-DE** und **NORDE**.

## JIS

Ein weiteres Layout ist JIS (kurz für Japanese Industrial Standard).
Dieses hat keine Kompabilität mit deutscher Beschriftung.

# Profile

Die Tastenkappen können unterschiedliche geformt sein, dies wird als Profile bezeichnet.
Tasten einer Reihe sind normalerweise gleich geformt, aber die Formen unterschiedlicher Reihen unterscheiden sich.

Die Formen können sich in der Höhe, Neigung und Ausgestaltung der Fingerauflage unterscheiden.

Einige Profile sind _uniform_, was bedeutete, dass alle Tastenreihen die gleiche Form aufweisen. Das hat zur Folge, dass man nicht auf die physikalische Position einer Taste angewiesen ist, um die Keycaps gemäß der eigenen Sprache oder Wünsche anzuordnen.

[Diese Bilder](https://imgur.com/a/E7m0l8j#SRqCVjD) zeigen unterschiedliche Profile und ermöglichen einen guten Vergleich zur Höhe.
Bei [keycaps.info](https://www.keycaps.info/) können die Profile interaktiv verglichen und übereinander gelegt werden.

# Wie bunt darf es sein?

Am häufigsten findet man [per Spritzguss](https://youtu.be/siI-cw0y048) hergestellte Teile aus Kunststoff.
Einzelstücke sind oftmals auch aus anderen Materialien, bspw. Aluminium, Kunstharz, Holz, Beton oder Keramik.

## Blanks

Bei der Verwendung von unbedruckten Tastenkappen ist das Layout oftmals egal.
Geachtet werden muss auf ein passendes Enter und die Länge einiger Tasten, gegebenfalls in der zugehörigen Reihe.

## Bedruckte Keycaps

Das Bedrucken von Keycaps ist vergleichsweise einfach und macht es für Hersteller einfach auch deutsche Umlaute anzubieten.

Gänge Bezeichnungen bedruckter Keycaps sind **Dye-Sublimination** (kurz **Dye-Sub**) oder **Reverse Dye-Sub**.

## Mehrfarbiger Spritzguss

Fast alle Keycaps werden im Spritzgussverfahren hergestellt.
Es lassen sich ein einem mehrschrittigen Verfahren auch die Zeichen auf den Tastenkappen gießen.
Dies erfordert Formen mit dem entsprechenden Profil und Berücksichtigung der gewünschten Beschriftung.
Die Herstellung entsprechender Formen ist relativ aufwändig und teuer.
Nicht jeder Hersteller hat die benötigten Formen für Buchstaben der deutschen Sprache.

Werden mehrfarbige Tastenkappen im Spritzgussverfahren hergestellt, handelt es sich dabei meistens um zweifarbige Kombinationen.
Da hier zweimal jeweils unterschiedliche gefärbtes Plastik in die Formen gespritzt wird, nennt man diese Variante **Doubleshot**.

Seltener findet man dreifarbige Kombinationen, diese werden **Tripleshot** genannt.

Genauere Beschreibungen des Verfahrens und dessen Eigenheiten finden sich [bei Deskthority](https://deskthority.net/wiki/Double-shot_moulding).

