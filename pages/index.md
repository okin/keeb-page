<!--
.. title: Herzlich willkommen!
.. slug: index
.. date: 2022-06-18 12:50:05 UTC+02:00
.. description: Deine Seite für Informationen zu mechanischen Tastaturen!
.. type: text
-->

Willkommen auf _Keebs, Keebs, Keebs!_, deiner Seite für Informationen zu mechanischen Tastaturen in deutscher Sprache.

Das Ziel dieser Informationssammlung ist unabhängig und unaufgeregt Informationen über das Hobby der Mechanische Tastaturen bereitzustellen, ohne den Clickbait von Influencern oder den Verkaufsabsichten von Shops.

Obwohl Tastaturen für die meisten von uns ein alltäglicher Gegenstand sind - von der Eingabe auf dem Handy, über die Arbeit am Computer bis hin zur Bedienung hochkomplexer Maschinerie - sind Tastaturen mit mechanischen Schaltern oftmals eine Seltenheit.
Tastaturen sind unsere Schnittstelle zu elektronischen Geräten aller Art und mechanische Tastaturen erlauben uns einen hohen Grad der Anpassung, von Aussehen über Ergonomie bis hin zum Klang. 

Mechanische Tastaturen sind ein sehr vielseitiges und vielschichtiges Hobby.
Du findest hier eine Übersicht über [gängige Begriffe](/glossar/) des Hobbies, wichtige [Communities](/communities/), [Medien](/medien/) und mehr.

Falls du diese Seite auf einem Mobilgerät anschaust, findest du das Menü hinter dem Kreis am unteren Ende der Seite.

Diese Seite wird [auf GitLab.com/okin/keeb-page](https://gitlab.com/okin/keeb-page) gepflegt. Du kannst dich gerne an den Inhalten beteiligen!
