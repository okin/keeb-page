<!--
.. title: Firmware
.. slug: firmware
.. date: 2022-01-12 21:37:41 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Eine Firmware sorgt - vereinfacht gesagt - dafür, dass der Druck auf deiner Tastatur registriert und die entsprechende Information an den Computer weitergeleitet wird.

Jede Tastatur hat eine Firmware, einige Tastaturen erlauben darüber hinaus diese auszutauschen.
Mit einer austauschbaren Firmware lässt sich das Verhalten der Tastatur, bspw. die Tastenbelegung oder Lichteffekte, nach belieben anpassen.


# Beliebte Firmwares

* [QMK](https://qmk.fm/) ist eine der beliebtesten und weitverbreitesten Firmwares. Sie ist Open Source, unterstützt eine große Menge an [Tastaturen] und hat viele Features. QMK ist kabelgebundene Tastaturen beschränkt. Eigene Layouts werden programmiert, was eine große Freiheit, aber auch ein gewissen Vorwissen benötigt. Tools wie der [QMK Configurator](https://config.qmk.fm) oder [QMK Toolbox](https://github.com/qmk/qmk_toolbox) machen QMK einer breiteren Masse zugänglich.
* [Vial](https://get.vial.today/) ist eine Oberfläche zur Firmware-Programmierung, welche im Hintergrund wiederum auf _QMK_ aufsetzt. Die Besonderheit von Vial ist, dass die Tastenbelegung direkt zur Laufzeit geändert werden kann, was das Ausprobieren von neuen Belegungen extrem einfach macht.
* [BlueMicro](https://bluemicro.jpconstantineau.com/) ist eine Firmware für kabellose Tastaturen, welche über Bluetooth Low Energy (BLE) kommuniziert. Sie ist eine komplette Eigenentwicklung und aktuell nur für Eigenbauten verwendbar.
